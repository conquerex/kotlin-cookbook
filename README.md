# Kotlin Programming Cookbook
This is the code repository for [Kotlin Programming Cookbook](https://www.packtpub.com/application-development/kotlin-programming-cookbook?utm_source=github&utm_medium=repository&utm_campaign=9781788472142), published by [Packt](https://www.packtpub.com/?utm_source=github). It contains all the supporting project files necessary to work through the book from start to finish.
## About the Book
The Android team has announced first-class support for Kotlin 1.1. This acts as an added boost to the language and more and more developers are now looking into Kotlin for their application development. This recipe-based book will be your guide to grasping the Kotlin Programming language.

The recipes in this book build from simple language concepts to more complex applications of the language. After the fundamentals of the language, you will learn how to apply the object-oriented programming features of Kotlin 1.1. Programming with Lambdas will show you how to use the functional power of Kotlin.

This book has recipes that will get you started with Android programming with Kotlin 1.1, providing quick solutions to common problems encountered during Android app development. You will also be taken through recipes that will teach you microservice and concurrent programming with Kotlin. Going forward, you will learn to test and secure your applications with Kotlin. Finally, this book supplies recipes that will help you migrate your Java code to Kotlin and will help ensure that it's interoperable with Java.

## Instructions and Navigation

* Some of the codes are hosted on Gitlab and the link to them are mentioned in the "Getting ready" section of the recipe.
* Some codes are self-explanatory and can be tested by merely copy-pasting into the IDE.
* If you face any problem in code, or recipe, feel free to contact me at aanandshekharroy[at]gmail[dot]com

The code will look like the following:
```
fun main(args: Array<String>) {
      var x:Int = if(10>20)  5  else  10
    println("$x")
 }
```

This book assumes familiarity with Java and Android development. This is not an introductory book for learning Kotlin. Readers must have used Android studio because many of the recipes will be focused toward Android development.

## Related Products
* [Reactive Programming in Kotlin](https://www.packtpub.com/application-development/reactive-programming-kotlin?utm_source=github&utm_medium=repository&utm_campaign=9781788473026)

* [Programming Kotlin](https://www.packtpub.com/application-development/programming-kotlin?utm_source=github&utm_medium=repository&utm_campaign=9781787126367)

* [Kotlin for Beginners: Learn Programming With Kotlin [Video]](https://www.packtpub.com/application-development/kotlin-beginners-learn-programming-kotlin-video?utm_source=github&utm_medium=repository&utm_campaign=9781788625944)